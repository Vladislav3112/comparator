package com.example.itschool.commentlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    PostAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerView = new RecyclerView(this);

        Post [] posts = new Post[4];
        posts[0] = new Post("post1",10,1000);
        posts[1] =  new Post("post2",100,10090);
        posts[2] = new Post("post3",18,890);
        posts[3] = new Post("post4",1800,890000);
        posts[4] = new Post("post5",10,1000);

        adapter = new PostAdapter(this, posts);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(adapter);

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        String selested = spinner.getSelectedItem().toString();
        if (selested == "Likes"){
            Arrays.sort(posts, new SortedByLikes());
        }

        if (selested == "Views"){
            Arrays.sort(posts, new SortedByViews());
        }


        setContentView(recyclerView);
    }


}
