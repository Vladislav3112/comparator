package com.example.itschool.commentlist;

import android.content.Context;

import java.util.Comparator;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
public class Post {
    private String name;
    private  int likes;
    private  int views ;
    private String img;

    public  Post (String name, int likes,int views) {
        this.name = name;
        this.likes = likes;
        this.views = views;
    }


    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getPostname() {
        return name;
    }

    public void setPostname(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }




}
