package com.example.itschool.commentlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.CommentViewHolder> {

    Post[] posts;
    Context context;

    PostAdapter(Context context, Post[] list) {
        this.context = context;
        this.posts = list;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Log.d("MYLOG", "create view holder");
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_layout, null);
        CommentViewHolder commentView = new CommentViewHolder(itemLayoutView);
        return commentView;
    }

    @Override
    public void onBindViewHolder(CommentViewHolder viewHolder, int i) {
        Log.d("MYLOG", "bind view holder");
        Post post = posts[i];
        viewHolder.mTVname.setText(post.getPostname());
    }

    @Override
    public int getItemCount() {
        return 5;
    }


    class CommentViewHolder extends RecyclerView.ViewHolder {
        ImageView mIVAvatar;
        TextView mTVname,likes,views;

        public CommentViewHolder(View rootView) {
            super(rootView);
            mIVAvatar = (ImageView) rootView.findViewById(R.id.iv_avatar);
            mTVname = (TextView) rootView.findViewById(R.id.tv_name);
            likes = (TextView) rootView.findViewById(R.id.tv_likes);
            views = (TextView) rootView.findViewById(R.id.tv_views);
            likes.setText(String.valueOf(Post.getLikes())+="likes");
            views.setText(String.valueOf(Post.getViews())+="views");

        }
    }


}
